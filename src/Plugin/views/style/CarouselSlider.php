<?php

/**
 * @file
 * Definition of Drupal\carouselslider\Plugin\views\style\CarouselSlider.
 */

namespace Drupal\carouselslider\Plugin\views\style;

use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\display\DisplayPluginBase;

/**
 * Style plugin uses views ui to configure views data for rendering charts.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "carouselslider",
 *   title = @Translation("CarouselSlider"),
 *   module = "carouselslider",
 *   theme = "views_view_unformatted",
 *   help = @Translation("Display the resulting data set as a carouselslider"),
 *   display_types = {"normal"}
 * )
 */

class CarouselSlider extends StylePluginBase {
  /**
   * Does the style plugin allows to use style plugins.
   *
   * @var bool
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * Do not use grouping.
   *
   * @var bool
   */
  protected $usesGrouping = FALSE;

  /**
   * Do not use grouping.
   *
   * @var array
   */
  protected $optionsTypes = array();

  /**
   * {@inheritdoc}
   */
  public function renderGroupingSets($sets, $level = 0) {
    $output = parent::renderGroupingSets($sets);
    foreach ($output as &$set) {
      $set['#attached']['library'][] = 'carouselslider/slick-views';
      $view_name = $this->view->storage->id();
      $view_settings['display'] = $this->view->current_display;
      $view_settings['viewname'] = $view_name;

      foreach ($this->options['carousel'] as $name => $value) {
        $types = $this->optionsTypes;
        if ($types[$name] && $types[$name] == 'checkbox') {
          $view_settings['slick'][$name] = (bool) $value;
        }
        else {
          $view_settings['slick'][$key] = $value;
        }
      }

      $carouselslider_id = 'carouselslider-' . $view_name . '-' . $this->view->current_display;
      $set['#attached']['js'][] = array('data' => array('carouselslider' => array($carouselslider_id => $view_settings)), 'type' => 'setting');

    }

    return $output;
  }

  /**
   * Set default options.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['carousel']['contains'] = $this->defineOptionsWithType();
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    foreach ($this->defineOptionsWithType() as $name => $definition) {
      $this->optionsTypes[$name] = $definition['type'];
    }


  }

  /**
   * Provide type information in addition to the option defaults.
   */
  protected function defineOptionsWithType() {

    $options['arrows'] = array(
      'default' => TRUE,
      'type' => 'checkbox',

    );

    $options['accessibility'] = array(
      'default' => TRUE,
      'type' => 'checkbox',
    );

    $options['dots'] = array(
      'default' => TRUE,
      'type' => 'checkbox',
    );
    $options['autoplay'] = array(
      'default' => FALSE,
      'type' => 'checkbox',
    );
    $options['centermode'] = array(
      'default' => FALSE,
      'type' => 'checkbox',
    );
    $options['draggable'] = array(
      'default' => TRUE,
      'type' => 'checkbox',
    );
    $options['fade'] = array(
      'default' => FALSE,
      'type' => 'checkbox',
    );
    $options['focusOnSelect'] = array(
      'default' => FALSE,
      'type' => 'checkbox',
    );
    $options['infinite'] = array(
      'default' => TRUE,
      'type' => 'checkbox',
    );
    $options['pauseOnHover'] = array(
      'default' => TRUE,
      'type' => 'checkbox',
    );
    $options['pauseOnDotsHover'] = array(
      'default' => TRUE,
      'type' => 'checkbox',
    );
    $options['slidesToShow'] = array(
      'default' => 1,
      'type' => 'checkbox',
    );
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, &$form_state) {
    parent::buildOptionsForm($form, $form_state);

    foreach ($this->displayHandler->getHandlers('field') as $field => $handler) {
      $options[] = $field;
    }

    $form['carousel'] = array(
      '#type' => 'fieldset',
      '#title' => t('Carousel settings'),
      '#collapsible' => FALSE,
    );
    $form['carousel']['arrows'] = array(
      '#title' => t('Display arrows'),
      '#description' => t('The carousel will display arrow buttons.'),
    );
    $form['carousel']['dots'] = array(
      '#title' => t('Dots'),
      '#description' => t('Show dots for each page of the carousel'),
    );
    $form['carousel']['accessibility'] = array(
      '#title' => t('Accessibility'),
      '#description' => t('Enable tabbing and arrow key navigation.'),
    );
    $form['carousel']['autoplay'] = array(
      '#title' => t('Autoplay'),
      '#description' => t('Autoplay carousel.'),
    );
    $form['carousel']['centermode'] = array(
      '#title' => t('Center mode'),
      '#description' => t("Keep selected slide in center."),
    );
    $form['carousel']['draggable'] = array(
      '#title' => t('Draggable slides'),
      '#description' => t("Slides can be dragged."),
    );
    $form['carousel']['fade'] = array(
      '#title' => t('Fade'),
      '#description' => t("Fade on switch."),
    );

    foreach ($form['carousel'] as $name => &$element) {
      if (is_array($element)) {
        $element['#default_value'] = $this->options['carousel'][$name];
        $element['#type'] = $this->optionsTypes[$name];
      }
    }
  }
}
