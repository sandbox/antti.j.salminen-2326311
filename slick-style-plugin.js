(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.carouselslider =  {
    attach: function(context) {
        if(drupalSettings.carouselslider){
          $.each(drupalSettings.carouselslider, function(id) {
            /* Our view settings */
            var viewname = this.viewname;
              var display = this.display;
              var settgs = this.slick;
              var havedots= {
                  dots: true,
                  accessibility: true,
                  arrows: false
              };
            /* the selectors we have to play with */
            var displaySelector = '.view-id-'+ viewname +'.view-display-id-'+
                    display+' > .view-content';
              jQuery(displaySelector).slick( this.slick	);

          });
                }
        }
    };
})(jQuery, Drupal, drupalSettings);
